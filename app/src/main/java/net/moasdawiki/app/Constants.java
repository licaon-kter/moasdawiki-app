/*
 * MoasdaWiki App
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation (GPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
 */

package net.moasdawiki.app;

/**
 * Defines constants and settings.
 */
public abstract class Constants {
    public static final String PREFERENCES_SYNC_SERVER_HOST = "sync_server_host";
    public static final String PREFERENCES_SYNC_SERVER_PORT = "sync_server_port";
    public static final String PREFERENCES_SYNC_SERVER_NAME = "sync_server_name";
    public static final String PREFERENCES_SYNC_SERVER_VERSION = "sync_server_version";
    public static final String PREFERENCES_SYNC_SERVER_HOST_DISPLAYNAME = "sync_server_host_displayname";
    public static final String PREFERENCES_SYNC_SERVER_SESSION_ID = "sync_server_session_id";
    public static final String PREFERENCES_SYNC_CLIENT_SESSION_ID = "sync_client_session_id";
    public static final String PREFERENCES_SYNC_SERVER_SESSION_AUTHORIZED = "sync_server_session_authorized";
    public static final String PREFERENCES_SYNC_SERVER_TIME = "last_sync_server_time";
    public static final String PREFERENCES_CALENDAR_ENABLED = "calendar_enabled";
}
