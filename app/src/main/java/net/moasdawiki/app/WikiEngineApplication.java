/*
 * MoasdaWiki App
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation (GPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
 */

package net.moasdawiki.app;

import android.app.Application;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Messages;
import net.moasdawiki.base.Settings;
import net.moasdawiki.server.RequestDispatcher;
import net.moasdawiki.service.handler.FileDownloadHandler;
import net.moasdawiki.service.handler.SearchHandler;
import net.moasdawiki.service.handler.ViewPageHandler;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.repository.RepositoryService;
import net.moasdawiki.service.search.SearchIgnoreList;
import net.moasdawiki.service.search.SearchIndex;
import net.moasdawiki.service.search.SearchService;
import net.moasdawiki.service.transform.IncludePageTransformer;
import net.moasdawiki.service.transform.KontaktseiteTransformer;
import net.moasdawiki.service.transform.TerminTransformer;
import net.moasdawiki.service.transform.TransformWikiPage;
import net.moasdawiki.service.transform.TransformerService;
import net.moasdawiki.service.transform.WikiTagsTransformer;
import net.moasdawiki.service.wiki.WikiService;

import java.io.File;

/**
 * Main control of the wiki App.
 *
 * Must be run globally for the App, i.e. outside of an activity.
 */
public class WikiEngineApplication extends Application {
    private static final String REPOSITORY_ROOT_PATH_DEFAULT = "repository";

    private Logger logger;
    private RepositoryService repositoryService;
    private Settings settings;
    private Messages messages;
    private WikiService wikiService;
    private SearchService searchService;
    private SynchronizeWikiClient synchronizeWikiClient;
    private TerminTransformer terminTransformer;
    private RequestDispatcher requestDispatcher;

    @Override
    public void onCreate() {
        super.onCreate();

        logger = new Logger(System.out);
        logger.write("MoasdaWiki App starting");

        File internalStorageRepositoryRoot = new File(getFilesDir(), REPOSITORY_ROOT_PATH_DEFAULT);

        // basic services
        repositoryService = new RepositoryService(logger, internalStorageRepositoryRoot, null, false);
        settings = new AndroidSettings(logger, repositoryService, Settings.getConfigFileApp());
        messages = new Messages(logger, settings, repositoryService);
        wikiService = new WikiService(logger, repositoryService, false);
        SearchIgnoreList searchIgnoreList = new SearchIgnoreList(logger, repositoryService);
        SearchIndex searchIndex = new SearchIndex(logger, repositoryService, wikiService, searchIgnoreList, true);
        searchService = new SearchService(logger, wikiService, searchIgnoreList, searchIndex, false);

        // App: use SynchronizeWikiClient instead of SynchronizationService
        synchronizeWikiClient = new SynchronizeWikiClient(this, logger, settings, repositoryService);

        // transformers
        // do not run the SynchronizationPageTransformer
        IncludePageTransformer includePageTransformer = new IncludePageTransformer(logger, wikiService);
        KontaktseiteTransformer kontaktseiteTransformer = new KontaktseiteTransformer();
        terminTransformer = new TerminTransformer(logger, messages, repositoryService, wikiService, false);
        WikiTagsTransformer wikiTagsTransformer = new WikiTagsTransformer(logger, settings, messages, wikiService);
        // list of transformers, the order matters
        TransformWikiPage[] transformers = {includePageTransformer, kontaktseiteTransformer, terminTransformer, wikiTagsTransformer};
        TransformerService transformerService = new TransformerService(transformers);

        // more services
        HtmlService htmlService = new HtmlService(logger, settings, messages, wikiService, transformerService);

        // HTTP handlers
        // do not run the EditorHandler
        ViewPageHandler viewPageHandler = new ViewPageHandler(logger, settings, wikiService, htmlService);
        SearchHandler searchHandler = new SearchHandler(logger, settings, messages, wikiService, searchService, htmlService);
        FileDownloadHandler fileDownloadHandler = new FileDownloadHandler(logger, settings, repositoryService, htmlService);
        requestDispatcher = new RequestDispatcher(htmlService, viewPageHandler,
                searchHandler, null, fileDownloadHandler, null);
    }

    public void resetServices() {
        repositoryService.reset();
        settings.reset();
        messages.reset();
        wikiService.reset();
        searchService.reset();
        terminTransformer.reset();
    }

    @Override
    public void onTerminate() {
        logger.write("MoasdaWiki app stopping");
        super.onTerminate();
    }

    public RepositoryService getRepositoryService() {
        return repositoryService;
    }

    public Settings getSettings() {
        return settings;
    }

    public TerminTransformer getTerminTransformer() {
        return terminTransformer;
    }

    public SynchronizeWikiClient getSynchronizeWikiClient() {
        return synchronizeWikiClient;
    }

    public RequestDispatcher getRequestDispatcher() {
        return requestDispatcher;
    }
}
